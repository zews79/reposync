#!/bin/bash

REPOPATH=/srv/redhat/repos
REPOFILE=local.repo
HOST=192.168.122.1

echo $REPOFILE
echo $REPOPATH
echo $HOST

for i in $(ls -d $REPOPATH/*/)
do

REPONAME=`basename ${i}`

cat << EOF >> $REPOFILE
[${REPONAME}]
name=${REPONAME}
#baseurl=file:///${i}
baseurl=http://${HOST}/${REPONAME}
enabled=1
gpgcheck=0

EOF

done
