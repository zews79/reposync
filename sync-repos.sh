#!/bin/bash

echo INSTALL DEPS
yum -y install createrepo yum-utils
echo START REPOSYNC
for i in $(cat /list); do reposync -n -t -d -l -p /repos/ -r ${i}; done
echo END REPOSYNC
echo ...
echo START REPOMANAGE
for i in $(ls -d /repos/*/); do rm $(repomanage --keep=1 --old ${i}); done
echo END REPOMANAGE
echo ...
echo START CREATEREPO
for i in $(ls -d /repos/*/); do createrepo -v ${i}; done
echo END CREATEREPO
